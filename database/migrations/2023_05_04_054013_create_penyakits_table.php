<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('penyakits', function (Blueprint $table) {
            $table->id();
            $table->string("nip")->nullable();
            $table->string("nama")->nullable();
            $table->string("tinggi_badan")->nullable();
            $table->string("berat_badan")->nullable();
            $table->string("lingkar_perut")->nullable();
            $table->string("kolesterol")->nullable();
            $table->string("asam_urat")->nullable();
            $table->string("gula_darah")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('penyakits');
    }
};
