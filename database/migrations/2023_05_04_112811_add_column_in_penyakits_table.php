<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('penyakits', function (Blueprint $table) {
            $table->enum("obat_Kolesterol", [
                "YA", 
                "TIDAK"
            ])->nullable()->after("gula_darah");
            $table->enum("obat_asam_urat", [
                "YA", 
                "TIDAK"
            ])->nullable()->after("obat_Kolesterol");
            $table->enum("obat_gula_darah", [
                "YA", 
                "TIDAK"
            ])->nullable()->after("obat_asam_urat");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('penyakits', function (Blueprint $table) {
            //
        });
    }
};
