<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <meta name="keywords" content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 5 admin, bootstrap 5, css3 dashboard, bootstrap 5 dashboard, Matrix lite admin bootstrap 5 dashboard, frontend, responsive bootstrap 5 admin template, Matrix admin lite design, Matrix admin lite dashboard bootstrap 5 dashboard template" />
  <meta name="description" content="Matrix Admin Lite Free Version is powerful and clean admin dashboard template, inpired from Bootstrap Framework" />
  <meta name="robots" content="noindex,nofollow" />
  <!-- <title>Matrix Admin Lite Free Versions Template by WrapPixel</title> -->
  <title>Formulir Pendataan - Formulir</title>
  <!-- Favicon icon -->
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets')}}/images/logo-kabupaten.png" />
  <!-- Custom CSS -->
  <link rel="stylesheet" type="text/css" href="{{asset('assets')}}/libs/select2/dist/css/select2.min.css" />
  <link rel="stylesheet" type="text/css" href="{{asset('assets')}}/libs/jquery-minicolors/jquery.minicolors.css" />
  <link rel="stylesheet" type="text/css" href="{{asset('assets')}}/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" />
  <link rel="stylesheet" type="text/css" href="{{asset('assets')}}/libs/quill/dist/quill.snow.css" />
  <link href="../dist/css/style.min.css" rel="stylesheet" />
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
  <!-- ============================================================== -->
  <!-- Preloader - style you can find in spinners.css -->
  <!-- ============================================================== -->
  <div class="preloader">
    <div class="lds-ripple">
      <div class="lds-pos"></div>
      <div class="lds-pos"></div>
    </div>
  </div>
  <!-- ============================================================== -->
  <!-- Main wrapper - style you can find in pages.scss -->
  <!-- ============================================================== -->
  <div id="main-wrapper" data-layout="vertical" data-navbarbg="skin5" data-sidebartype="full" data-sidebar-position="absolute" data-header-position="absolute" data-boxed-layout="full">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <header class="topbar" data-navbarbg="skin5">
      <nav class="navbar top-navbar navbar-expand-md navbar-dark">
        <div class="navbar-header" data-logobg="skin5">
          <!-- ============================================================== -->
          <!-- Logo -->
          <!-- ============================================================== -->
          <a class="navbar-brand" href="index.html">
            <!-- Logo icon -->
            <b class="logo-icon ps-2">
              <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
              <!-- Dark Logo icon -->
              <img src="{{asset('assets')}}/images/logo-kabupaten.png" alt="homepage" class="light-logo" width="25" />
            </b>
            <!--End Logo icon -->
            <!-- Logo text -->
            <span class="logo-text ms-2">
              <!-- dark Logo text -->
              <!-- <img
                  src="{{asset('assets')}}/images/logo-text.png"
                  alt="homepage"
                  class="light-logo"
                /> -->
                <span>SI Kesehatan Pegawai</span>
            </span>
            <!-- Logo icon -->
            <!-- <b class="logo-icon"> -->
            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
            <!-- Dark Logo icon -->
            <!-- <img src="{{asset('assets')}}/images/logo-text.png" alt="homepage" class="light-logo" /> -->

            <!-- </b> -->
            <!--End Logo icon -->
          </a>
          <!-- ============================================================== -->
          <!-- End Logo -->
          <!-- ============================================================== -->
          <!-- ============================================================== -->
          <!-- Toggle which is visible on mobile only -->
          <!-- ============================================================== -->
          <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
          <!-- ============================================================== -->
          <!-- toggle and nav items -->
          <!-- ============================================================== -->
          <ul class="navbar-nav float-start me-auto">
            <li class="nav-item d-none d-lg-block">
              <a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a>
            </li>
            <!-- ============================================================== -->
            <!-- create new -->
            <!-- ============================================================== -->
            <!-- <li class="nav-item dropdown">
                <a
                  class="nav-link dropdown-toggle"
                  href="#"
                  id="navbarDropdown"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <span class="d-none d-md-block"
                    >Create New <i class="fa fa-angle-down"></i
                  ></span>
                  <span class="d-block d-md-none"
                    ><i class="fa fa-plus"></i
                  ></span>
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><a class="dropdown-item" href="#">Action</a></li>
                  <li><a class="dropdown-item" href="#">Another action</a></li>
                  <li><hr class="dropdown-divider" /></li>
                  <li>
                    <a class="dropdown-item" href="#">Something else here</a>
                  </li>
                </ul>
              </li> -->
            <!-- ============================================================== -->
            <!-- Search -->
            <!-- ============================================================== -->
            <!--  <li class="nav-item search-box">
                <a
                  class="nav-link waves-effect waves-dark"
                  href="javascript:void(0)"
                  ><i class="mdi mdi-magnify fs-4"></i
                ></a>
                <form class="app-search position-absolute">
                  <input
                    type="text"
                    class="form-control"
                    placeholder="Search &amp; enter"
                  />
                  <a class="srh-btn"><i class="mdi mdi-window-close"></i></a>
                </form>
              </li> -->
          </ul>
          <!-- ============================================================== -->
          <!-- Right side toggle and nav items -->
          <!-- ============================================================== -->
          <ul class="navbar-nav float-end">
            <!-- ============================================================== -->
            <!-- Comment -->
            <!-- ============================================================== -->
            <!-- <li class="nav-item dropdown">
                <a
                  class="nav-link dropdown-toggle"
                  href="#"
                  id="navbarDropdown"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  <i class="mdi mdi-bell font-24"></i>
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><a class="dropdown-item" href="#">Action</a></li>
                  <li><a class="dropdown-item" href="#">Another action</a></li>
                  <li><hr class="dropdown-divider" /></li>
                  <li>
                    <a class="dropdown-item" href="#">Something else here</a>
                  </li>
                </ul> -->
            </li>
            <!-- ============================================================== -->
            <!-- End Comment -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Messages -->
            <!-- ============================================================== -->
            <li class="nav-item dropdown">

            </li>
            <!-- ============================================================== -->
            <!-- End Messages -->
            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->

            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->
          </ul>
        </div>
      </nav>
    </header>
    <!-- ============================================================== -->
    <!-- End Topbar header -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <aside class="left-sidebar" data-sidebarbg="skin5">
      <!-- Sidebar scroll-->
      <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
          <ul id="sidebarnav" class="pt-4">
            <!-- <li class="sidebar-item">
                <a
                  class="sidebar-link waves-effect waves-dark sidebar-link"
                  href="index.html"
                  aria-expanded="false"
                  ><i class="mdi mdi-view-dashboard"></i
                  ><span class="hide-menu">Dashboard</span></a
                >
              </li> -->
            <li class="sidebar-item">
              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/" aria-expanded="false"><i class="mdi mdi-note-outline"></i><span class="hide-menu">Form Pendataan</span></a>
            </li>
            @if(!session()->has('username'))
            <li class="sidebar-item">
              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('auth-login')}}" aria-expanded="false"><i class="mdi mdi-note-outline"></i><span class="hide-menu">Login</span></a>
            </li>
            @else
            <li class="sidebar-item">
              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('rekap')}}" aria-expanded="false"><i class="mdi mdi-note-outline"></i><span class="hide-menu">Rekap</span></a>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('rekap-kadis')}}" aria-expanded="false"><i class="mdi mdi-note-outline"></i><span class="hide-menu">Rekap Kadis</span></a>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('process-logout')}}" aria-expanded="false"><i class="mdi mdi-note-outline"></i><span class="hide-menu">Logout</span></a>
            </li>
            @endif
          </ul>
        </nav>
        <!-- End Sidebar navigation -->
      </div>
      <!-- End Sidebar scroll-->
    </aside>
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
      <!-- ============================================================== -->
      <!-- Bread crumb and right sidebar toggle -->
      <!-- ============================================================== -->
      <div class="page-breadcrumb">
        <div class="row">
          <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Form Pendataan</h4>
            <div class="ms-auto text-end">
              <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">
                    Library
                  </li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
      <!-- ============================================================== -->
      <!-- End Bread crumb and right sidebar toggle -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- Container fluid  -->
      <!-- ============================================================== -->
      <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <form class="form-horizontal" id="form-create" method="POST" action="{{route('penyakit.store')}}">
                @csrf
                <div class="card-body">
                  <input type="hidden" name="is_kadis" id="is_kadis">
                  <input type="hidden" name="data_user" id="data_user">
                  <h4 class="card-title">Informasi</h4>
                  <div class="form-group row">
                    <label for="lname" class="col-sm-3 text-end control-label col-form-label">NIP (ASN) / NIK (UMUM)</label>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" id="nip" name="nip" placeholder="NIP/NIK Disini" required/>
                      @error('nip')
                        <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                    <button type="button" id="btn-search" class="btn btn-success col-sm-3">Cari</button>
                   
                  </div>
                  <div class="form-group row">
                    <label for="lname" class="col-sm-3 text-end control-label col-form-label">Nama</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Disini"  readonly required/>
                      @error('nama')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  <!-- <hr> -->
                  <div class="form-group row">
                    <label for="lname" class="col-sm-3 text-end control-label col-form-label">Berat Badan (Kg)</label>
                    <div class="col-sm-9">
                      <input required type="number" class="form-control" id="lname" name="berat_badan" placeholder="Berat Badan Disini" />
                      @error('berat_badan')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="email1" class="col-sm-3 text-end control-label col-form-label">Tinggi Badan (cm)</label>
                    <div class="col-sm-9">
                      <input required type="number" class="form-control" id="email1" name="tinggi_badan" placeholder="Tinggi Badan Disini" />
                      @error('tinggi_badan')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="cono1" class="col-sm-3 text-end control-label col-form-label">Lingkar Perut</label>
                    <div class="col-sm-9">
                      <input required type="number" class="form-control" id="cono1" name="lingkar_perut" placeholder="Lingkar Perut Disini" />
                      @error('lingkar_perut')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="cono1" class="col-sm-3 text-end control-label col-form-label">Tekanan Darah</label>
                    <div class="col-sm-9">
                      <input required type="text" class="form-control" id="cono1" name="tekanan_darah" placeholder="Tekanan Darah Disini" />
                      @error('tekanan_darah')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="cono1" class="col-sm-3 text-end control-label col-form-label">Kolesterol</label>
                    <div class="col">
                      <input required type="number" class="form-control" id="cono1" name="kolesterol" placeholder="Nilai" />
                      @error('kolesterol')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="cono1" class="col-sm-3 text-end control-label col-form-label">Asam Urat</label>
                    <div class="col">
                      <input required type="number" class="form-control" id="cono1" name="asam_urat" placeholder="Nilai" />  
                      @error('asam_urat')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="cono1" class="col-sm-3 text-end control-label col-form-label">Gula Darah</label>
                    <div class="col">
                      <input required type="number" class="form-control" id="cono1" name="gula_darah" placeholder="Nilai" />
                      @error('gula_darah')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="cono1" class="col-sm-3 text-end control-label col-form-label">Obat yang dikonsumsi</label>
                    <div class="col">
                      <div class="form-check p-1">
                          <div class="col m-1">
                            <label class="form-check-label mb-0" for="customControlAutosizing1">Penurun Kolesterol</label>
                            <input type="checkbox" class="form-check-input" name="obat_kolesterol" id="" />
                          </div>
                          <div class="col m-1">
                            <label class="form-check-label mb-0" for="customControlAutosizing1">Penurun Asam Urat</label>
                            <input type="checkbox" class="form-check-input" name="obat_asam_urat" id="" />
                          </div>
                          <div class="col m-1">
                            <label class="form-check-label mb-0" for="customControlAutosizing1">Penurun Gula Darah</label>
                            <input type="checkbox" class="form-check-input" name="obat_gula_darah" id=""  />
                          </div>
                          <div class="col m-1">
                            <label class="form-check-label mb-0" for="customControlAutosizing1">Penurun Tekanan Darah Tinggi</label>
                            <input type="checkbox" class="form-check-input" name="obat_tekanan_darah" id=""  />
                          </div>
                         
                        </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="cono1" class="col-sm-3 text-end control-label col-form-label">Informasi Tambahan</label>
                    <div class="col">
                      <textarea name="catatan" id="catatan" name="catatan" class="form-control"></textarea>
                      @error('catatan')
                          <div class="alert alert-danger">{{ $message }}</div>
                      @enderror
                    </div>
                  </div>
                </div>
                <div class="border-top">
                  <div class="card-body">
                    <button type="submit" id="btn-submit" class="btn btn-primary">
                      Simpan
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <!-- editor -->
        <!-- <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Quill Editor</h4> -->
        <!-- Create the editor container -->
        <!-- <div id="editor" style="height: 300px">
                    <p>Hello World!</p>
                    <p>Some initial <strong>bold</strong> text</p>
                    <p>
                      <br />
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div> -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
      </div>
      <!-- ============================================================== -->
      <!-- End Container fluid  -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- footer -->
      <!-- ============================================================== -->
      <footer class="footer text-center">
        All Rights Developed by
        <a href="https://diskominfo.mojokertokab.go.id/">Diskominfo Mojokerto</a>.
      </footer>
      <!-- ============================================================== -->
      <!-- End footer -->
      <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Page wrapper  -->
    <!-- ============================================================== -->
  </div>
  <!-- ============================================================== -->
  <!-- End Wrapper -->
  <!-- ============================================================== -->
  <!-- ============================================================== -->
  <!-- All Jquery -->
  <!-- ============================================================== -->
  <script src="{{asset('assets')}}/libs/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap tether Core JavaScript -->
  <script src="{{asset('assets')}}/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <!-- slimscrollbar scrollbar JavaScript -->
  <script src="{{asset('assets')}}/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
  <script src="{{asset('assets')}}/extra-libs/sparkline/sparkline.js"></script>
  <!--Wave Effects -->
  <script src="../dist/js/waves.js"></script>
  <!--Menu sidebar -->
  <script src="../dist/js/sidebarmenu.js"></script>
  <!--Custom JavaScript -->
  <script src="../dist/js/custom.min.js"></script>
  <!-- This Page JS -->
  <script src="{{asset('assets')}}/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
  <script src="../dist/js/pages/mask/mask.init.js"></script>
  <script src="{{asset('assets')}}/libs/select2/dist/js/select2.full.min.js"></script>
  <script src="{{asset('assets')}}/libs/select2/dist/js/select2.min.js"></script>
  <script src="{{asset('assets')}}/libs/jquery-asColor/dist/jquery-asColor.min.js"></script>
  <script src="{{asset('assets')}}/libs/jquery-asGradient/dist/jquery-asGradient.js"></script>
  <script src="{{asset('assets')}}/libs/jquery-asColorPicker/dist/jquery-asColorPicker.min.js"></script>
  <script src="{{asset('assets')}}/libs/jquery-minicolors/jquery.minicolors.min.js"></script>
  <script src="{{asset('assets')}}/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="{{asset('assets')}}/libs/quill/dist/quill.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script>
    //***********************************//
    // For select 2
    //***********************************//
    let csrfToken = $('meta[name="csrf-token"]').attr('content');
    $("#btn-submit").prop('disabled', true);
    @if(session()->has('success'))
    Swal.fire(
      'Berhasil!',
      '{{ session()->pull("success") }}',
      'success'
    )
    
    @endif
    /*colorpicker*/
    $(".demo").each(function() {

      //
      // Dear reader, it's actually very easy to initialize MiniColors. For example:
      //
      //  $(selector).minicolors();
      //
      // The way I've done it below is just for the demo, so don't get confused
      // by it. Also, data- attributes aren't supported at this time...they're
      // only used for this demo.
      //
      $(this).minicolors({
        control: $(this).attr("data-control") || "hue",
        position: $(this).attr("data-position") || "bottom left",

        change: function(value, opacity) {
          if (!value) return;
          if (opacity) value += ", " + opacity;
          if (typeof console === "object") {
            console.log(value);
          }
        },
        theme: "bootstrap",
      });
    });
    /*datwpicker*/
    jQuery(".mydatepicker").datepicker();
    jQuery("#datepicker-autoclose").datepicker({
      autoclose: true,
      todayHighlight: true,
    });
    var quill = new Quill("#editor", {
      theme: "snow",
    });

    $("#btn-search").on('click', function () {
        let nip = $("#nip").val();
        $.ajax({
          type: 'POST',
          url: '{{route("penyakit.search")}}',
          data: {
            nip:nip
          },
          headers: {
            'X-CSRF-TOKEN': csrfToken // menambahkan header CSRF Token
          },  
          success: function(response) {
            // menampilkan pesan sukses pada user
            console.log(Object.keys(response).length);
            if (Object.keys(response).length !==0) {
              
              if(response.status==false){
                $("#nama").prop('readonly', false);
                $("#btn-submit").prop('disabled', false);
                Swal.fire(
                  'Perhatian!',
                  'Data pegawai tidak tersedia, silahkan input manual pada kolom nama',
                  'info'
                )
                $("#nip").prop('readonly', false);
              }else{
                $("#nama").prop('readonly', true);
                $("#nip").prop('readonly', true);
                $("#btn-submit").prop('disabled', false);
                $("#nama").val(response.data.profil.nama_lengkap?response.data.profil.nama_lengkap:response.data.profil.namalengkap);
                $("#is_kadis").val(response.data.profil.is_kadis);
                $("#data_user").val(JSON.stringify(response.data));
                
              }
            }else{
              $("#nama").prop('readonly', true);
              $("#btn-submit").prop('disabled', true);
            }

          },
          error: function(xhr) {
            // menampilkan pesan error pada user
            alert('Terjadi kesalahan saat memproses data.');
          }
        });
    });

    </script>
    <script>
    $(document).ready(function() {
      //set initial state.
      $('#kolesterol').hide();

      $('#textbox1').change(function() {
        if (this.checked) {
          $("#kolesterol").show();
          // var returnVal = confirm("Are you sure?");
          // $(this).prop("checked", returnVal);
        } else {
          $("#kolesterol").hide();
        }
        // $('#textbox1').val(this.checked);        
      });
    });
  </script>

 

 

</body>

</html>