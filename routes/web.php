<?php

use App\Http\Controllers\PenyakitController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\AuthController;
use App\Models\penyakit;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [PenyakitController::class, "create"]);
// Route::get('/lihat', [PenyakitController::class, "lihat"]);
Route::get('/rekap', [PenyakitController::class, "rekapKadis"])->name("rekap");
Route::get('/rekap-kadis', [PenyakitController::class, "rekapNonKadis"])->name("rekap-kadis");
Route::get('/auth-login', [AuthController::class, "index"])->name("auth-login");
Route::post('/process-login', [AuthController::class, "login"])->name("process-login");
Route::get('/process-logout', [AuthController::class, "logout"])->name("process-logout");
Route::get('/penyakit', [PenyakitController::class, "index"])->name("penyakit.index");
Route::post('/penyakit', [PenyakitController::class, "store"])->name("penyakit.store");
Route::get('/penyakit/{penyakit}', [PenyakitController::class, "edit"])->name("penyakit.edit");
Route::put('/penyakit/{penyakit}', [PenyakitController::class, "update"])->name("penyakit.update");
Route::delete('/penyakit/{penyakit}', [PenyakitController::class, "destroy"])->name("penyakit.destroy");
Route::post('/search/penyakit', [PenyakitController::class, "searchUser"])->name('penyakit.search');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__ . '/auth.php';
