<?php

namespace App\Http\Controllers;

use App\Http\Requests\PenyakitCreateRequest;
use App\Http\Requests\PenyakitUpdateRequest;
use App\Models\penyakit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class PenyakitController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = penyakit::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
       
        // return $data;
        return view('form-pendataan');
    }

    public function searchUser(Request $request)
    {
        // return "data";
        $username = $request->nip;
        $user = Http::withBasicAuth('epegawai', '457KrKhLwqF9kE8RZuszNZBA3Wg84OAO')
        ->get('http://api.pegawai.mojokertokab.go.id/v2/pegawai/detail/' . $username);
        $data = json_decode($user->body());
        return response()->json($data
        , 200);
        
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->all();
        $ressData = penyakit::where("nip", "=", $validated["nip"]);
        if ($ressData->count()>=1) {
            $data = [
                "is_kadis"=>$validated["is_kadis"],
                "nama"=>strtoupper($validated["nama"]),
                "user_data"=>$validated["data_user"],
                "tinggi_badan"=>$validated["tinggi_badan"],
                "lingkar_perut"=>$validated["lingkar_perut"],
                "berat_badan"=>$validated["berat_badan"],
                "tekanan_darah"=>$validated["tekanan_darah"],
                "kolesterol"=>$validated["kolesterol"],
                "asam_urat"=>$validated["asam_urat"],
                "gula_darah"=>$validated["gula_darah"],
                "catatan"=>$validated["catatan"],
            ];
            if (isset($validated["obat_kolesterol"])) {
                $data["obat_kolesterol"] = $validated["obat_kolesterol"]=="on"?"YA":"TIDAK";
            }else{
                $data["obat_kolesterol"] = null;
            }
            if (isset($validated["obat_asam_urat"])) {
                $data["obat_asam_urat"] = $validated["obat_asam_urat"]=="on"?"YA":"TIDAK";
            }else{
                $data["obat_asam_urat"] = null;
            }
            if (isset($validated["obat_gula_darah"])) {
                $data["obat_gula_darah"] = $validated["obat_gula_darah"]=="on"?"YA":"TIDAK";
            }else{
                $data["obat_gula_darah"] = null;
            }
            if (isset($validated["obat_tekanan_darah"])) {
                $data["obat_tekanan_darah"] = $validated["obat_tekanan_darah"]=="on"?"YA":"TIDAK";
            }else{
                $data["obat_tekanan_darah"] = null;
            }
            $ressData->update($data);
        } else {
        
        $data = [
            "nip"=>$validated["nip"],
            "is_kadis"=>$validated["is_kadis"],
            "nama"=>strtoupper($validated["nama"]),
            "user_data"=>$validated["data_user"],
            "tinggi_badan"=>$validated["tinggi_badan"],
            "lingkar_perut"=>$validated["lingkar_perut"],
            "berat_badan"=>$validated["berat_badan"],
            "tekanan_darah"=>$validated["tekanan_darah"],
            "kolesterol"=>$validated["kolesterol"],
            "asam_urat"=>$validated["asam_urat"],
            "gula_darah"=>$validated["gula_darah"],
            "catatan"=>$validated["catatan"],
        ];
        if (isset($validated["obat_kolesterol"])) {
            $data["obat_kolesterol"] = $validated["obat_kolesterol"]=="on"?"YA":"TIDAK";
        }
        if (isset($validated["obat_asam_urat"])) {
            $data["obat_asam_urat"] = $validated["obat_asam_urat"]=="on"?"YA":"TIDAK";
        }
        if (isset($validated["obat_gula_darah"])) {
            $data["obat_gula_darah"] = $validated["obat_gula_darah"]=="on"?"YA":"TIDAK";
        }
        if (isset($validated["obat_tekanan_darah"])) {
            $data["obat_tekanan_darah"] = $validated["obat_tekanan_darah"]=="on"?"YA":"TIDAK";
        }
        
        // dd($data);
        penyakit::create($data);
        }
        

        return redirect()->back()->with([
            'success' => 'Data Berhasil Disimpan!'
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $item = penyakit::findOrFail($id)->get();
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $item = penyakit::findOrFail($id)->get();
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PenyakitUpdateRequest $request, string $id)
    {
        $item = $request->validated();
        $data = [
            "nip" => $item["nip"],
            "nama" => $item["nama"],
            "tinggi_badan" => $item["tinggi_badan"],
            "lingkar_perut" => $item["lingkar_perut"],
            "tekanan_darah" => $item["tekanan_darah"],
            "kolesterol" => json_encode([
                "kolesterol_nilai" => $item["kolesterol_nilai"],
                "kolesterol_obat" => $item["kolesterol_obat"],
                "kolesterol_obat_pendamping" => $item["kolesterol_obat_pendamping"],
            ]),
            "asam_urat" => json_encode([
                "asam_urat_nilai" => $item["asam_urat_nilai"],
                "asam_urat_obat" => $item["asam_urat_obat"],
                "asam_urat_obat_pendamping" => $item["asam_urat_obat_pendamping"],
            ]),
            "gula_darah" => json_encode([
                "gula_darah_nilai" => $item["gula_darah_nilai"],
                "gula_darah_obat" => $item["gula_darah_obat"],
                "gula_darah_obat_pendamping" => $item["gula_darah_obat_pendamping"],
            ]),
        ];
        $penyakit = penyakit::findOrFail($id);
        $penyakit->update($data);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $penyakit = penyakit::findOrFail($id);
        $penyakit->delete();
    }

    public function lihat(Request $request)
    {
        if (session()->has('username')) {
            $penyakit = penyakit::get();
            // foreach ($penyakit as $data) {
            //     print($data);
            // }
            return view('table-pendataan', compact('penyakit'));
        }else{
            return redirect("/");
        }
    }

    public function cek(Request $request)
    {
        $kadis = $request->cek;
        $rekap = $request->data_penyakit;

        if ($kadis = "ya" && $rekap = "kolesterol") {
            $pernyakit = penyakit::where([
                ['is_kadis', '!=', null],
                // ['']
            ])->get();
        }
    }

    public function rekapKadis(Request $request)
    {
        // return $penyakit = penyakit::distinct()->get([
        // "nip",
        // "is_kadis",
        // "nama",
        // "user_data",
        // "tinggi_badan",
        // ]);
        // "nip",
        // "is_kadis",
        // "nama",
        // "user_data",
        // "tinggi_badan",
        // "lingkar_perut",
        // "berat_badan",
        // "tekanan_darah",
        // "kolesterol",
        // "asam_urat",
        // "gula_darah",
        // "obat_kolesterol",
        // "obat_asam_urat",
        // "obat_gula_darah",
        // "obat_tekanan_darah"
        // DB::table('penyakit')->distinct('nip')->get();
        // foreach ($penyakit as $data) {
            //     print($data);
            // }
        if (session()->has('username')) {
            $penyakit = penyakit::where('is_kadis', '=', null)->get();
            return view('table-pendataan-kadis', compact('penyakit'));
            // foreach ($penyakit as $data) {
            //     print($data);
            // }
            return view('table-pendataan', compact('penyakit'));
        }else{
            return redirect("/");
        }
    }

    public function rekapNonKadis(Request $request)
    {
        if (session()->has('username')) {
            $penyakit = penyakit::where('is_kadis', '!=', null)->get();
            // foreach ($penyakit as $data) {
            //     print($data);
            // }
            return view('table-pendataan-nonkadis', compact('penyakit'));
        }else{
            return redirect("/");
        }
        
    }
}
