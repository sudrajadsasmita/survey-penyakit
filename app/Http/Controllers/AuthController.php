<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view("login");
    }
    public function login(Request $request)
    {
        if ($request->username=="admin"&& $request->password=="admin") {
            session()->put('username', 'admin');
            return redirect("/");
        }else{
            return redirect()->route('auth-login')->with([
                'alert-login' => 'Periksa kembali username dan password anda!'
            ]);
        }
    }
    public function logout()
    {
        session()->forget('username');
        return redirect("/");

    }
}
