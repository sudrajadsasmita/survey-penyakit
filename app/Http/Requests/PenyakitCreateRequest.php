<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PenyakitCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            "nip" => "required|string",
            "nama" => "required|string",
            "is_kadis" => "required|string",
            "data_user" => "required|string",
            "tinggi_badan" => "required|string",
            "berat_badan" => "required|string",
            "lingkar_perut" => "required|string",
            "tekanan_darah" => "required|string",
            "kolesterol" => "required|string",
            "asam_urat" => "required|string",
            "gula_darah" => "required|string",
            "obat_minum_kolesterol" => "nullable|string",
            "obat_minum_asam_urat" => "nullable|string",
            "obat_minum_gula_darah" => "nullable|string",
        ];
    }
}
