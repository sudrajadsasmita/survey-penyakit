<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PenyakitUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            "nip" => "required|string",
            "tinggi_badan" => "required|string",
            "berat_badan" => "required|string",
            "linkar_perut" => "required|string",
            "tekanan_darah" => "required|string",
            "kolesterol_nilai" => "required|string",
            "kolesterol_obat" => "nullable|string",
            "kolesterol_obat_pendamping" => "nullable|string",
            "asam_urat_nilai" => "required|string",
            "asam_urat_obat" => "nullable|string",
            "asam_urat_obat_pendamping" => "nullable|string",
            "gula_darah_nilai" => "required|string",
            "gula_darah_obat" => "nullable|string",
            "gula_darah_obat_pendamping" => "null|string",
        ];
    }
}
