<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class penyakit extends Model
{
    use HasFactory;
    protected $fillable = [
        "nip",
        "is_kadis",
        "nama",
        "user_data",
        "tinggi_badan",
        "lingkar_perut",
        "berat_badan",
        "tekanan_darah",
        "kolesterol",
        "asam_urat",
        "gula_darah",
        "obat_kolesterol",
        "obat_asam_urat",
        "obat_gula_darah",
        "obat_tekanan_darah",
        "catatan"
    ];

   
}
